import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from "@remix-run/react";
import { Partytown } from '@builder.io/partytown/react';

export const meta = () => ({
  charset: "utf-8",
  title: "New Remix App",
  viewport: "width=device-width,initial-scale=1",
});

export default function App() {
  return (
    <html>
      <head>
        <Meta />
        <Links />

        <Partytown debug={ true } />

        <script
          type="text/partytown"
          src='/posthog.js'
        />
      </head>
      <body>
        <Outlet />
        <ScrollRestoration />
        <Scripts />
        <LiveReload />

      </body>
    </html>
  );
}
